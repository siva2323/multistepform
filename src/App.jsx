import SignUpPage from "./components/SignUpPage";
import "./index.css";

function App() {
    return (
        <>
            <div className="d-flex flex-row justify-content-center p-5 vh-100 vw-100">
                <div className="d-flex flex-row shadow-lg rounded-5"
                    style={{
                        width: "90%",
                    }}  >
                    <img
                        src="img.png"
                        alt="login"
                        className=" rounded-5"
                        style={{ width: "30%" }}
                    />
                    <div className="d-flex flex-column w-100 ">
                        <SignUpPage />
                    </div>
                </div>
            </div>
        </>
    );
}

export default App;
