import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUserData } from "../features/userSlice";
import MessagePage from "./MessagePage";
import { RiErrorWarningLine } from "react-icons/ri";

const SignUpPage = () => {
    const userData = useSelector((state) => state.user);

    let initialValues = {
        FirstName: userData.userInfo.FirstName ? userData.userInfo.FirstName : "",
        LastName: userData.userInfo.LastName ? userData.userInfo.LastName : "",
        DateOfBirth: userData.userInfo.DateOfBirth ? userData.userInfo.DateOfBirth : "",
        Email: userData.userInfo.Email ? userData.userInfo.Email : "",
        Address: userData.userInfo.Address ? userData.userInfo.Address : "",
    };

    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);

    const dispatch = useDispatch();

    let userInfos = {
        FirstName: formValues?.FirstName,
        LastName: formValues?.LastName,
        DateOfBirth: formValues?.DateOfBirth,
        Email: formValues?.Email,
        Address: formValues?.Address,
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setFormErrors(validate(formValues));
        setIsSubmit(true);
    };

    const validate = (formValuesFromSubmission) => {
        let errorsOccured = {};
        const mailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const lettersRegex = /^[A-Za-z]+$/;

        if (!formValuesFromSubmission.FirstName) {
            errorsOccured.FirstName = {};
        } else if (!lettersRegex.test(formValuesFromSubmission.FirstName)) {
            errorsOccured.FirstNameInvalid = {};
        }

        if (!formValuesFromSubmission.LastName) {
            errorsOccured.LastName = {};
        } else if (!lettersRegex.test(formValuesFromSubmission.LastName)) {
            errorsOccured.LastNameInvalid = {};
        }

        if (!formValuesFromSubmission.DateOfBirth) {
            errorsOccured.DateOfBirth = {};
        }

        if (!formValuesFromSubmission.Email) {
            errorsOccured.Email = {};
        } else if (!mailRegex.test(formValuesFromSubmission.Email)) {
            errorsOccured.EmailInvalid = {};
        }

        if (!formValuesFromSubmission.Address) {
            errorsOccured.Address = {};
        }
        return errorsOccured;
    };

    if (Object.keys(formErrors).length === 0 && isSubmit) {
        return <MessagePage />;
    }

    return (
        <div className=" p-4 pt-3" style={{ width: "95%" }}>
            <div className="d-flex flex-row  justify-content-start w-100 ">
                <p className="w-25">
                    <button type="button" className="btn btn-primary btn-square-md mx-2 border-0">
                        1
                    </button>
                    Sign Up
                </p>
                <p className="text-muted mx-3 w-25">
                    <button type="button" className="btn btn-light btn-square-md mx-2 border-0">
                        2
                    </button>{" "}
                    Message
                </p>
                <p className="text-muted w-25">
                    <button type="button" className="btn btn-light btn-square-md mx-2 border-0">
                        3
                    </button>
                    Checkbox
                </p>
            </div>
            <hr className="mx-2" />
            <div className="container">
                <p className="text-secondary">Step 1/3</p>
                <h4>
                    <strong
                        style={{
                            fontFamily: "monospace",
                        }}
                    >
                        Sign UP
                    </strong>
                </h4>
                <div className="">
                    <form className="d-flex " onSubmit={handleSubmit}>
                        <div>
                            <div className="row ">
                                <div className="col-6">
                                    <small className={`${formErrors?.FirstName ? "text-danger" : formErrors?.FirstNameInvalid ? "text-danger" : ""}`}>First Name</small>
                                    {formErrors.FirstName || formErrors.FirstNameInvalid ? (
                                        <i className="text-danger">
                                            <RiErrorWarningLine />
                                        </i>
                                    ) : (
                                        ""
                                    )}
                                    <input
                                        type="text"
                                        className={`rounded-2 container border ${formErrors?.FirstName ? "border-danger" : formErrors.FirstNameInvalid ? "border-danger" : ""} `}
                                        style={{
                                            height: "40px",
                                            paddingLeft: "10px",
                                            backgroundColor: `${formErrors?.FirstName ? "#f5ebec" : formErrors?.FirstNameInvalid ? "#f5ebec" : ""}`,
                                        }}
                                        value={formValues?.FirstName}
                                        name="FirstName"
                                        onChange={handleChange}
                                    />
                                    <small className="text-danger">{formErrors?.FirstName ? "First Name is required!" : formErrors?.FirstNameInvalid ? "Invalid First Name" : ""}</small>
                                </div>
                                <div className="col-6">
                                    <small className={`${formErrors?.LastName ? "text-danger" : formErrors?.LastNameInvalid ? "text-danger" : ""}`}>Last Name</small>
                                    {formErrors.LastName || formErrors.LastNameInvalid ? (
                                        <i className="text-danger">
                                            <RiErrorWarningLine />
                                        </i>
                                    ) : (
                                        ""
                                    )}
                                    <input
                                        type="text"
                                        className={`rounded-2 container border ${formErrors?.LastName ? "border-danger" : formErrors.LastNameInvalid ? "border-danger" : ""} `}
                                        style={{
                                            height: "40px",
                                            paddingLeft: "10px",
                                            backgroundColor: `${formErrors?.LastName ? "#f5ebec" : formErrors?.LastNameInvalid ? "#f5ebec" : ""}`,
                                        }}
                                        value={formValues?.LastName}
                                        name="LastName"
                                        onChange={handleChange}
                                    />
                                    <small className="text-danger">{formErrors?.LastName ? "Last Name is required!" : formErrors?.LastNameInvalid ? "Invalid Last Name" : ""}</small>
                                </div>
                                <div className="col-6">
                                    <small className={`${formErrors?.DateOfBirth ? "text-danger" : ""}`}>Date Of Birth</small>
                                    {formErrors.DateOfBirth ? (
                                        <i className="text-danger">
                                            <RiErrorWarningLine />
                                        </i>
                                    ) : (
                                        ""
                                    )}
                                    <input
                                        type="date"
                                        className={`rounded-2 container border ${formErrors?.DateOfBirth && "border-danger"}`}
                                        style={{
                                            height: "40px",
                                            paddingLeft: "10px",
                                            backgroundColor: `${formErrors?.DateOfBirth ? "#f5ebec" : ""}`,
                                        }}
                                        value={formValues?.DateOfBirth}
                                        name="DateOfBirth"
                                        onChange={handleChange}
                                    />
                                    <small className="text-danger">{formErrors?.DateOfBirth ? "DOB is required!" : ""}</small>
                                </div>
                                <div className="col-6">
                                    <small className={`${formErrors?.Email ? "text-danger" : formErrors?.EmailInvalid ? "text-danger" : ""}`}>Email Address</small>
                                    {formErrors.Email || formErrors.EmailInvalid ? (
                                        <i className="text-danger">
                                            <RiErrorWarningLine />
                                        </i>
                                    ) : (
                                        ""
                                    )}
                                    <input
                                        type="text"
                                        className={`rounded-2 container border ${formErrors?.Email ? "border-danger" : formErrors.EmailInvalid ? "border-danger" : ""} `}
                                        style={{
                                            height: "40px",
                                            paddingLeft: "10px",
                                            backgroundColor: `${formErrors?.Email ? "#f5ebec" : formErrors?.EmailInvalid ? "#f5ebec" : ""}`,
                                        }}
                                        value={formValues?.Email}
                                        name="Email"
                                        onChange={handleChange}
                                    />
                                    <small className="text-danger">{formErrors?.Email ? "Email is required!" : formErrors?.EmailInvalid ? "Invalid Email!" : ""}</small>
                                </div>
                                <div className="w-100">
                                    <div>
                                        <small className={`${formErrors?.Address ? "text-danger" : ""}`}>Address</small>
                                        {formErrors.Address ? (
                                            <i className="text-danger">
                                                <RiErrorWarningLine />
                                            </i>
                                        ) : (
                                            ""
                                        )}
                                        <input
                                            type="text"
                                            className={`rounded-2 container border ${formErrors?.Address && "border-danger"}`}
                                            style={{
                                                height: "40px",
                                                paddingLeft: "10px",
                                                backgroundColor: `${formErrors?.Address ? "#f5ebec" : ""}`,
                                            }}
                                            value={formValues?.Address}
                                            name="Address"
                                            onChange={handleChange}
                                        />
                                        <p className="text-danger">
                                            <small>{formErrors?.Address && "Address is required!"}</small>
                                        </p>
                                    </div>
                                </div>
                                <hr />
                                <div className="d-flex flex-wrap justify-content-end">
                                    <button
                                        className="btn btn bg-primary d-flex flex-wrap justify-content-center text-light p-3"
                                        style={{
                                            width: "150px",
                                            height: "60px",
                                        }}
                                        onClick={() => dispatch(getUserData(userInfos))}
                                    >
                                        NextStep
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default SignUpPage;
