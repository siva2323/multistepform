import React from "react";

const DisplayMessage = () => {
    return (
        <div className="container d-flex flex-wrap p-5 justify-content-center ">
            <h1>Submitted successfully</h1>
        </div>
    );
};

export default DisplayMessage;
