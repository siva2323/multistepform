import React from "react";
import { getUserMessage } from "../features/userSlice";
import { useDispatch } from "react-redux";
import { useState } from "react";
import CheckboxPage from "./CheckboxPage";
import { useSelector } from "react-redux";
import SignUpPage from "./SignUpPage";
import { RiErrorWarningLine } from "react-icons/ri";

const MessagePage = () => {
    const userData = useSelector((state) => state.user);

    let initialValues = {
        Message: userData.userMessage.Message ? userData.userMessage.Message : "",
        userChoiceNumber: userData.userMessage.userChoiceNumber ? userData.userMessage.userChoiceNumber : "",
    };

    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);
    const [message, setMessage] = useState(initialValues);
    const [userChoice, setUserChoice] = useState(userData.userMessage.userChoiceNumber ? (userData.userMessage.userChoiceNumber === "The number one choice" ? true : false) : true);
    const [page, setPage] = useState(false);

    const dispatch = useDispatch();

    let userMessage = {
        Message: message.Message,
        userChoiceNumber: userChoice ? "The number one choice" : "The number two choice",
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setMessage({ ...message, [name]: value });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setFormErrors(validate(message.Message));
        setIsSubmit(true);
    };

    const validate = (messageData) => {
        let errorsOccured = {};
        if (messageData.length < 1) {
            errorsOccured.Message = {};
        }
        return errorsOccured;
    };

    if (Object.keys(formErrors).length === 0 && isSubmit) {
        return <CheckboxPage />;
    }

    if (page) {
        return <SignUpPage />;
    }

    let checkboxColor1;
    let checkboxColor2;

    if (userChoice) {
        checkboxColor1 = "text-secondary";
        checkboxColor2 = "";
    } else {
        checkboxColor2 = "text-secondary";
        checkboxColor1 = "";
    }

    return (
        <div className=" px-5 py-3" style={{ width: "85%" }}>
            <div className="d-flex flex-wrap justify-content-start w-100">
                <p className="text-muted">
                    <button
                        type="button"
                        style={{
                            backgroundColor: "#ebeff5",
                        }}
                        className="btn btn-light btn-square-md mx-2 border-0"
                    >
                        ✔️
                    </button>
                    Sign Up
                </p>
                <p className="mx-3">
                    <button type="button" className="btn btn-primary btn-square-md mx-2 border-0">
                        2
                    </button>{" "}
                    Message
                </p>
                <p className="text-muted">
                    <button type="button" className="btn btn-light btn-square-md mx-2 border-0">
                        {" "}
                        3
                    </button>
                    Checkbox
                </p>
            </div>
            <hr className="mx-2" />
            <form onSubmit={handleSubmit}>
                <div>
                    <p className="text-secondary "> step 2/3</p>
                    <h3
                        className="mb-2"
                        style={{
                            fontFamily: "monospace",
                        }}
                    >
                        <strong>Message</strong>
                    </h3>
                    <small className={`${formErrors.Message && "text-danger"}`}> Message</small>
                    {formErrors?.Message && (
                        <i className="text-danger">
                            <RiErrorWarningLine />
                        </i>
                    )}
                    <textarea
                        name="Message"
                        className={`rounded-2 p-3 border ${formErrors.Message && "border-danger"}`}
                        cols="40"
                        value={message.Message}
                        onChange={handleChange}
                        rows="5"
                        style={{
                            width: "100%",
                            backgroundColor: `${formErrors.Message && "#f5ebec"}`,
                        }}
                    ></textarea>
                    <small className="text-danger">{formErrors?.Message && "Message is required!"}</small>
                </div>
                <section className="d-flex flex wrap">
                    <div className="form-check">
                        <input className="form-check-input rounded-circle" type="checkbox" checked={userChoice} onChange={() => setUserChoice(!userChoice)} />
                        <label className={`form-check-label  ${checkboxColor2} `} htmlFor="flexCheckDefault">
                            {" "}
                            The number one choice
                        </label>
                    </div>
                    <div className="mx-5">
                        <input className="form-check-input rounded-circle" type="checkbox" checked={!userChoice} onChange={() => setUserChoice(!userChoice)} />
                        <label className={`form-check-label  ${checkboxColor1} `} htmlFor="flexCheckDefault">
                            {" "}
                            The number two choice
                        </label>
                    </div>
                </section>
                <hr />
                <div className="d-flex flex wrap justify-content-end">
                    <p
                        className="btn btn bg-primary d-flex flex-wrap justify-content-center bg-white p-3"
                        onClick={() => setPage(true)}
                        style={{
                            width: "150px",
                            height: "60px",
                        }}
                    >
                        {" "}
                        Back
                    </p>
                    <button
                        className="btn btn bg-primary d-flex flex-wrap justify-content-center text-light p-3"
                        style={{
                            width: "150px",
                            height: "60px",
                        }}
                        onClick={() => dispatch(getUserMessage(userMessage))}
                    >
                        Next Step{" "}
                    </button>
                </div>
            </form>
        </div>
    );
};

export default MessagePage;
