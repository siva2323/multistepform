import React, { useState } from "react";
import MessagePage from "./MessagePage";
import { useDispatch, useSelector } from "react-redux";
import DisplayMessage from "./DisplayMessage";
import { getUserDetail } from "../features/userSlice";
import "../index.css";

const CheckboxPage = () => {
    const userData = useSelector((state) => state.user);

    let initialvalue = {
        Image: userData.userDetail.Image ? userData.userDetail.Image : "",
        Choice: userData.userDetail.Choice ? userData.userDetail.Choice : "",
    };

    const [page, setPage] = useState(false);
    const [isSubmit, setIsSubmit] = useState(false);
    const [userChoice, setUserChoice] = useState(true);
    const [image, setImage] = useState("");
    const [formErrors, setFormErrors] = useState([]);
    const [count, setCount] = useState(0);

    const dispatch = useDispatch();

    let userDetail = {
        Choice: userChoice ? "I want to add this option." : "Let me click on this checkbox and choose some cool stuff.",
        Image: image,
    };

    if (page) {
        return <MessagePage />;
    }

    let checkboxColor1;
    let checkboxColor2;

    if (userChoice) {
        checkboxColor1 = "text-secondary";
        checkboxColor2 = "";
    } else {
        checkboxColor2 = "text-secondary";
        checkboxColor1 = "";
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        setFormErrors(validate(image));
        setIsSubmit(true);
        dispatch(getUserDetail(userDetail));
    };

    const handleClick = (event) => {
        setIsSubmit(true);
        dispatch(getUserDetail(userDetail));
        setFormErrors(validate(image));
    };

    const validate = (image) => {
        let errorsOccured = {};
        if (image.length < 1) {
            errorsOccured.picNotSelected = "Select a Picture";
        }
        return errorsOccured;
    };

    let image1 = "";
    let image2 = "";

    if (image === "Guy Image") {
        image1 = "border-primary";
        image2 = "";
    } else if (image === "Girl Image") {
        image2 = "border-primary";
        image1 = "";
    }

    if (Object.keys(formErrors).length === 0 && isSubmit && count === 0) {
        alert("Submitted Successfully");
        setCount(1);
    }

    return (
        <div className=" px-5 py-2" style={{ width: "85%" }}>
            <div className="d-flex flex-wrap justify-content-start w-100">
                <p className="text-muted">
                    <button
                        type="button"
                        style={{
                            backgroundColor: "#ebeff5",
                        }}
                        className="btn btn-light btn-square-md mx-2 border-0"
                    >
                        {" "}
                        ✔️
                    </button>{" "}
                    Sign Up
                </p>
                <p className="text-muted mx-3">
                    <button
                        type="button"
                        style={{
                            backgroundColor: "#ebeff5",
                        }}
                        className="btn btn-light btn-square-md mx-2 border-0"
                    >
                        {" "}
                        ✔️
                    </button>{" "}
                    Message
                </p>
                <p>
                    <button type="button" className="btn btn-primary btn-square-md mx-2 border-0">
                        {" "}
                        3
                    </button>{" "}
                    Checkbox
                </p>
            </div>
            <hr className="mx-2" />
            <div>
                <form onSubmit={(event) => handleSubmit(event)}>
                    <p className="text-secondary mt-5">step 3/3 </p>
                    <h3
                        className="mb-1"
                        style={{
                            fontFamily: "monospace",
                        }}
                    >
                        <strong>Checkbox</strong>
                    </h3>
                    <div className=" imageBox d-flex flex-wrap justify-content-between ">
                        <img
                            src="1.png"
                            className={` guyImage border border-2 ${image1} rounded `}
                            onClick={() => setImage("Guy Image")}
                            style={{
                                width: "45%",
                                height: "10%",
                            }}
                        />
                        <img
                            src="2.png"
                            className={` guyImage border border-2 ${image2} rounded `}
                            onClick={() => setImage("Girl Image")}
                            style={{
                                width: "45%",
                                height: "10%",
                            }}
                        />
                    </div>
                    <small className="text-danger">{formErrors?.picNotSelected ? formErrors.picNotSelected : ""}</small>
                    <div className="form-check my-3">
                        <input className="form-check-input rounded-circle" type="checkbox" checked={userChoice} onChange={() => setUserChoice(!userChoice)} />
                        <label className={`form-check-label  ${checkboxColor2} `} htmlFor="flexCheckDefault">
                            I want to add this option.
                        </label>
                    </div>
                    <div className="form-check mb-5">
                        <input checked={!userChoice} className="form-check-input rounded-circle" type="checkbox" onChange={() => setUserChoice(!userChoice)} />
                        <label className={`form-check-label  ${checkboxColor1} `} htmlFor="flexCheckDefault">
                            {" "}
                            Let me click on this checkbox and choose some cool stuff.
                        </label>
                    </div>
                    <hr />
                    <div className="d-flex flex wrap justify-content-end">
                        <p
                            className="btn btn bg-primary d-flex flex-wrap justify-content-center bg-white p-3"
                            style={{
                                width: "150px",
                                height: "60px",
                            }}
                            onClick={() => setPage(true)}
                        >
                            Back
                        </p>
                        <button
                            className="btn btn bg-primary d-flex flex-wrap justify-content-center text-light p-3"
                            style={{
                                width: "150px",
                                height: "60px",
                            }}
                            onClick={(event) => handleClick()}
                        >
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default CheckboxPage;
