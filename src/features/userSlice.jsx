import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
    name: "user",
    initialState: {
        userInfo: {},
        userMessage: {},
        userDetail: {},
    },

    reducers: {
        getUserData: (state, action) => {
            state.userInfo = action.payload;
        },
        getUserMessage: (state, action) => {
            state.userMessage = action.payload;
        },
        getUserDetail: (state, action) => {
            state.userDetail = action.payload;
        },
    },
});
export const { getUserData, getUserMessage, getUserDetail } = userSlice.actions;
export default userSlice.reducer;
